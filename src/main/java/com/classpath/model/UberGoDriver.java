package com.classpath.model;

public class UberGoDriver implements UberGo {
    @Override
    public double commute(String from, String destination) {
        System.out.println("Travelling with Vinod from "+ from + " to "+ destination);
        return Math.ceil(Math.random()* 50877);
    }
}